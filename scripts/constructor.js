//11111111111111
(function() {
	function Student() {
		this.name = 'kangjingjing';
		this.say = function() {
			console.log(this.name)
		}
	}

	var student = new Student();
	student.say() // ==> kangjingjing;
	var student1 = new Student();
	student1.name = 'chengjun';
	student1.say() // ==> chengjun;
})();
//2222222222222222222222
(function() {
	function Student() {}

	Student.prototype = {
		name: 'liangna',
		say: function() {
			console.log(this.name)
		}
	}

	var student = new Student();
	student.say() // ==> liangna;
})();

//3333333333333333
(function() {
	function Student() {}
	console.log(Student.prototype)
})();
//444444444444444444444444
(function() {
	function Student() {}
	Student.prototype.name = 'gale';
	Student.prototype.say = function() {
		console.log(this.name);
	}

	var student = new Student();
	student.say(); // ==> gale;
})();
//55555555555555555555555
(function() {
	function Student(name) {
		this.name = name
	}
	Student.prototype = {
		say: function() {
			console.log(this.name)
		}
	}

	var student = new Student('gewenbing');
	student.say(); //==>  gewenbing;
})();
//66666666666666666666
(function() {
	function Student(options) {
		this.options = options;
	}
	Student.prototype = {
		say: function() {
			console.log(this.options.name);
		}
	}

	var student = new Student({
		name: 'xiongshuguang'
	})

	student.say() // ==> xiongshuguang;
})()

