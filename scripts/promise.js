var Promise = require('promise');
var request = require('request');
var p = new Promise(function(resolve){
	request('http://www.baidu.com', function(err, res, body){
		resolve(body);
	});
}).then(function(baidu){
	return new Promise(function(resolve){
		request('http://www.so.com', function(err, res, body){
			resolve([baidu, body]);
		});
	})
}).then(function(res){
	console.log(res[0]);
	console.log('====================================================================');
	console.log(res[1]);
})
//console.log(p);
