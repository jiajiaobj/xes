//======变量提升========
(function() {
	console.log('x : ' + x); //=> undefined
	//console.log(y);
	var x;
	x = 2;
	console.log('x : ' + x); //=> 2;
})();

var v = 'Hello World';
(function() {
	console.log('v : ' + v); //=> undefined
	var v = 'I love you';
})();

//==========块级作用域======
(function() {
	var m = 1;
	console.log('m : ' + m); //=> 1
	if (true) {
		m = 2;
		console.log('m : ' + m); //=> 2
	}
	console.log('m : ' + m); //=> 2
})();

//==========函数提升======
(function() {
	fun(); //=> so gar
	function fun() {
		console.log('so gar');
	}
})();

(function() {
	//fun(); //=> error
	var fun = function() {
		console.log('so gar');
	}
})();

//=======值类型(数值、布尔值、null、undefined)=========
(function() {
	var str1 = 'hello'
	var str2 = str1;
	str1 += 'world';
	console.log(str2); //=> hello
})();

//=======引用类型(对象、数组、函数)=========
(function() {
	var arr1 = [1, 2];
	var arr2 = arr1;
	arr1.push(3);
	console.log(arr2);
})();

//=======判断变量是否定义==========
(function() {
	if(typeof(a) !== 'undefined') {
		console.log('a');
	}
})();

