var request = require('request');
var a;
var content;

///同步
function A(){
	a = 'look'
}
function getA(){
	console.log(a);
}
A();
getA();


///异步
function baidu(){
	request('http://www.baidu.com', function(err, res, body){
		content = body
		getBaidu(content);
	})
}
function getBaidu(baidu){
	console.log(baidu);
}
//baidu();


//嵌套
function baiduAndSo(){
	request('http://www.baidu.com', function(err, res, baidu){
		request('http://www.so.com', function(err, res, so){
			getBaiduAndSo(baidu, so);
		})
	})
}
function getBaiduAndSo(baidu, so ){
	console.log(baidu);
	console.log('=============================================================================');
	console.log(so);
}
baiduAndSo();
