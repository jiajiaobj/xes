define(function(require, exports, module) {
	var cookie = require('lib/cookie');
	var Url = require('lib/url');
	var html = require('text!/tpl/teacher/login.html');
	var img = require('image!/img/login-bg.png');
	var Base = require('base');
	var Index = Base.extend({
		init: function() {
			this.view('index');
		},
		view: function(id) {
			var __self__ = this;
			var token = cookie.get('token');
			if (token) {
				window.location.href = '/#teacher';
				return;
			}
			this.render({
				tpl: html,
				data: {}
			});
		},
		bind: function(vm) {
			$('#signIn').bind('click', function() {
				var mobile = $('#mobile').val();
				var password = $('#password').val();
				/*$.post('/api/login',{
					mobile: mobile,
					password: password
				}, function(data) {
				},
				'json');*/
				$.post('http://api.cloudclass.local/user/login', {
					mobile: mobile,
					password: password
				},
				function(data) {
					if (data.status) {
						cookie.set('token', data.token);
						cookie.set('uid', data.data.id);
					} else {
						alert('用户名密码不匹配,请重新输入');
						return;
					}
					$('#login').addClass('animated rotateOut');
					setTimeout(function() {
						if (window.webAdapter && window.webAdapter.loginResponse) {
							window.webAdapter.loginResponse({
								code: 1,
								id: data.data.uid,
								name: data.data.name,
								type: 'teacher',
								token: data.token
							})
						}
						location.href = "http://localhost/#teacher";
						/*return;
						var url = new Url('http://115.28.244.55:8085/oauth2/authorize');
						url.query.client_id = 'test_live_pc';
						url.query.redirect_uri = 'http://www.lewaijiao.com';
						url.query.response_type = 'code';
						url.query.theme = 'default';
						url.query.role = 'student';
						url.query.state = 123456;
						//console.log(url.href)
						location.href = url.href;*/
					},
					1000)
				});
			});
		}
	});
	return Index;
});

