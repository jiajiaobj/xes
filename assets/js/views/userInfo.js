define(function(require, exports, module) {
	var html = require('text!/tpl/userInfo.html');
	var Base = require('base');
	var Index = Base.extend({
		init: function() {
			this.view();
		},
		view: function(id) {
			var __self__ = this;
			$.get('/getUserInfo',{
				userId:1,
				userType:'student'
			},function(data){			
				__self__.render({
					el:"#userInfo",
					tpl: html,
					data: {
						url:data.data.img_url,
						name: data.data.name,
						level: data.data.level,
					}
				});
			});			
		},
		bind: function(vm) {
		}
	});
	return Index;
});

