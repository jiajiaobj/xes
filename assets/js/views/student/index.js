define(function(require, exports, module) {
	var html = require('text!/tpl/student/index.html');
	var Base = require('base');

	var userInfo = require(['views/userInfo'],function(Index){
		new Index();
	});

	var Index = Base.extend({
		init: function() {
			this.view();
		},
		view: function(id) {
			var __self__ = this;
			var today=new Date();
			var day=today.getFullYear()+"-"+(parseInt(today.getMonth())+1)+"-"+today.getDate();


			$.when($.get('/getUserInfo',{
				userId:1,
				userType:'student'
			}), $.get('/getCourses',{
				startDate: day,
				endDate: day,
				userId: 1,
				userType: 'teacher'
			})).done(function(data1, data2){
				var today_course=data2[0].data.course[day];
				var today_date=today.getFullYear()+"/"+(parseInt(today.getMonth())+1)+"/"+today.getDate()+" ";
				var today_ms=today.getTime();
				var current=0;
				var have_course=1;				

				if(today_course!=undefined){
					//开始上课前10分钟，<a>链接可用
					for(var i=0;i<today_course.length;i++){
						var startTime=today_date+today_course[i].startTime;
						var endTime=today_date+today_course[i].endTime;

						startTime=(new Date(startTime)).getTime();
						endTime=(new Date(endTime)).getTime();

						if(today_ms<endTime){
							current=i;	
							today_course[i].is_prepare=1;
							break;						
						}
					}
					for(var i=0;i<today_course.length;i++){
						if(i!=current){
							today_course[i].is_prepare=0;		
						}
					}				
				}else{
					have_course=0;
				}

				__self__.render({
					tpl: html,
					data: {
						url: data1[0].data.img_url,
						name: data1[0].data.name,
						level: data1[0].data.level,

						//name: data2[0].data.name,
						//level: data2[0].data.level,
						weekend: data2[0].data.weekend,
						date: data2[0].data.date,
						courseId: current,
						courseCounts: data2[0].data.course.length,
						course: data2[0].data.course[day],
						has_course:have_course,
					}
				});
			
			});

			$.get('/getCourses',{
				startDate:day,
				endDate:day,
				userId:1,
				userType:'student'
			},function(data){
			});			
		},
		bind: function(vm) {
			$(document).on("click","#course-btn",function(){
				location.href=$(this).attr("data-href");
			});
				
				
			setInterval(function(){	
				var today=new Date();
				var today_course=vm.$data.course;
				var today_date=today.getFullYear()+"/"+(parseInt(today.getMonth())+1)+"/"+today.getDate()+" ";
				var today_ms=today.getTime();
				var current=0;
				var have_course=1;			

				if(today_course!=undefined){
					//开始上课前10分钟，<a>链接可用
					for(var i=0;i<today_course.length;i++){
						var startTime=today_date+today_course[i].startTime;
						var endTime=today_date+today_course[i].endTime;

						startTime=(new Date(startTime)).getTime();
						endTime=(new Date(endTime)).getTime();

						if(today_ms<endTime){
							current=i;
							today_course[i].is_prepare=1;
							// vm.$data.course[i].is_prepare=1;
							if((startTime-today_ms)>10*60*1000){
								$("#course-btn").attr("disabled",true);			
							}else{
								$(".course-status").html("立即进入");
								$("#course-btn").attr("disabled",false);
							}
							break;						
						}
					}
					for(var i=0;i<today_course.length;i++){
						if(i!=current){
							today_course[i].is_prepare=0;
							// vm.$data.course[i].is_prepare=0;	
						}
					}				
				}else{
					have_course=0;
				}
			},1000);

			$('#calendar').lncalendar({
				source: '/getCourses',
				type: 'month'
			});
			$('#course-pager').on('click', '#btn-prev', function() {
				vm.$data.courseId--;
				if (vm.$data.courseId <= 0) {
					vm.$data.courseId = 0;
				}
			});
			$('#course-pager').on('click', '#btn-next', function() {
				vm.$data.courseId++;
				if (vm.$data.courseId >= vm.$data.course.length) {
					vm.$data.courseId = vm.$data.course.length - 1;
				}
			});
		}
	});
	return Index;
});

