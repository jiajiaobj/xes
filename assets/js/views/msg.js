(function () {
    var w = window,
        d = document;

    w.CHAT = {
        contentObj:d.getElementById("content"),
        screenheight:w.innerHeight ? w.innerHeight : dx.clientHeight,
        username:null,
        userid:null,
        socket:null,

        //第一个界面用户提交用户名
        login:function(username,userid){
            //var username = "usertest";
			//var userid = "23455435";
			if(username != ""){
                this.init(username,userid);
            }
            else{
                return false;
            }
        },

        //退出，本例只是一个简单的刷新
        logout:function(){
            this.socket.disconnect();
        },

        //提交聊天消息内容
        sendMsg:function(content){
            if(content != ''){
                var obj = {
                    userid: this.userid,
                    username: this.username,
                    content: content
                };
                this.socket.emit('message', obj,function (data) {
                    if (data) {
                        //发送成功
                        addSendMsgDisplayItem(content);
                    }
                    else {
                        //发送失败
                    }
                });
            }
            return false;
        },

        init:function(username,userid){
            /*
             客户端根据时间和随机数生成uid,这样使得聊天室用户名称可以重复。
             实际项目中，如果是需要用户登录，那么直接采用用户的uid来做标识就可以
             */
            this.userid = this.genUid();//暂时的
            this.username = username;

            //连接websocket后端服务器
            this.socket = io.connect('ws://realtime.plhwin.com:3000');
            //this.socket = io.connect('123.57.2.227:3000');

            //监听连接失败
            this.socket.on('connect_failed', function(o){
                //alert("connect failed");
                //chatWebAdapter.connectFailed();
            });

            //监听连接成功
            this.socket.on('connect', function(o){
                //alert("connect success");
                //chatWebAdapter.connectSuccess();
            });

            //告诉服务器端有用户登录
            this.socket.emit('login', {userid:this.userid, username:this.username});

            //监听新用户登录
            this.socket.on('login', function(o){
                //chatWebAdapter.loginResponse();
            });

            //监听用户退出
            this.socket.on('logout', function(o){
            });

            //监听消息发送
            this.socket.on('message', function(obj){
                //chatWebAdapter.onChatMessage(obj.username,obj.content);
                var isme = (obj.userid == CHAT.userid) ? true : false;
                if(!isme){
                    w.CHAT.addReceiveMsgDisplayItem(obj.content);
                }
				else{
					w.CHAT.addSendMsgDisplayItem(obj.content);
				}
            });

            //监听连接断开
            this.socket.on('disconnect', function () {
                try {
                    this.socket.reconnect();
                } catch (e) {
                }
            });
        },

        //在界面增加一条自己发送的消息
        addSendMsgDisplayItem:function(msg){
            var html = '<div class="sender"><div><img src="';

            var picurl = '/img/msg.jpg';
            html += picurl;
            html += '" /></div><div><div class="left_triangle"></div><div class="left_rectangle">';
            html += msg;
            html += '</div></div></div>';
            html += '<div style="clear:both"></div>';//清一下格式，使后面的margin_top有效

            d.getElementById('content').insertAdjacentHTML("beforeEnd",html);
            this.scrollToBottom();
        },

        //在界面增加一条收到的消息
        addReceiveMsgDisplayItem:function(msg){
            var html = '<div class="receiver"><div><img src="';

            var picurl = '/img/msg.jpg';
            html += picurl;
            html += '" /></div><div><div class="right_triangle"></div>';
            html += '<div class="right_rectangle"><span>';
            html += msg;
            html += '</span></div></div></div>';
            html += '<div style="clear:both"></div>';//清一下格式，使后面的margin_top有效

            d.getElementById('content').insertAdjacentHTML("beforeEnd",html);
            this.scrollToBottom();
        },

        //让浏览器滚动条保持在最低部
        scrollToBottom:function(){
            var contentObjtmp = d.getElementById("content");
            w.scrollTo(0, contentObjtmp.offsetHeight);
        },

        genUid:function(){
            return new Date().getTime()+""+Math.floor(Math.random()*899+100);
        },

        //提交聊天消息内容，在网页中使用
        sendMsgHtml:function(){
            var content = d.getElementById("msgcontend").value;

            if(content == ''){
                content = '你好！你好！';
            }
            if(content != ''){
                d.getElementById("msgcontend").value = '';
                var obj = {
                    userid: this.userid,
                    username: this.username,
                    content: content
                };
                this.socket.emit('message', obj,function (data) {
                    if (data) {
                        //发送成功
                        addSendMsgDisplayItem(content);
                    }
                    else {
                        //发送失败
                    }
                });
            }
            return false;
        }
    };

})();
