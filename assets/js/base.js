define(function(require, exports, module) {
	var Class = require('lib/inherit');
	var template = require('lib/template');

	var Hasher = require('lib/hasher');
	var Crossroads = require('lib/crossroads');
	var Vue = require('lib/vue');

	var Base = Class.extend({
		init: function() {
			//添加app路由要放在Hash配置之前
			//否则无法记住首次打开app
			//this.apps();
			Base.Hash.initialized.add(this.parseHash);
			Base.Hash.changed.add(this.parseHash);
			Base.Hash.init();
		},
		//参数自由灵活一些
		//obj.tpl 	模板，可以是字符串，也可以是url
		//obj.data	数据源,可以是object类型，也可以是url
		//obj.el	目标元素，如果有，则渲染到目标元素，如果没有，则渲染到默认容器
		render: function(obj, fn) {
			var __self__ = this;
			var tpl;
			var data;
			var el;
			var config;
			var isTplUrl = false; //记录下来obj.tpl是不是一个Url
			var isDataUrl = false; //记录下来obj.data是不是一个url
			if (/\.html$/.test(obj.tpl)) {
				tpl = $.get(obj.tpl);
				isTplUrl = true;
			} else {
				tpl = obj.tpl;
			}
			if (typeof(obj.data) == 'string') {
				data = $.ajax(obj.data);
				isDataUrl = true;
			} else {
				data = obj.data;
			}
			if (typeof(obj.el) == 'undefined') {
				el = '#content';
			} else {
				el = obj.el;
			}
			$.when(tpl, data).done(function(data1, data2) {
				var data_tpl;
				var data_data;
				var vm;
				if (isTplUrl) {
					data_tpl = data1[0];
				} else {
					data_tpl = data1;
				}
				if (isDataUrl) {
					data_data = data2[0];
				} else {
					data_data = data2;
				}
				$(el).html(data_tpl);
				config = $.extend(obj, {
					el: el,
					data: data_data
				});
				vm = new Vue(config);
				if(fn){
					fn(vm);
				}
				__self__.bind(vm);
			});
		},
		bind: function(vm){},
		/*render: function(obj, tpl, data) {
			var html;
			if (arguments.length < 3) {
				data = tpl || {};
				tpl = obj;
				obj = null;
			}
			html = template(tpl, data);
			if (obj) {
				$(obj).html(html);
			} else {
                $('.g-bd').fadeOut('fast',function(){
                    $('.g-bd').html(html).fadeIn('fast');
                });
			}
			return html;
		},*/
		parseHash: function(hash, bak) {
			var uri = hash;
			var root = Base.Route.addRoute('/{id}');
			if (uri == '') {
				require(['views/index'], function(Index) {
					new Index(hash);
				});
				return;
			}
			if (root.match(hash)) {
				require(['views/' + hash + '/index'], function(Index) {
					new Index(hash);
				});
				return;
			}
			require(['views/' + hash], function(Page) {
				new Page();
			});
		}
		//添加app路由机制
		//每个app通过单独的app id进行控制
		/*apps: function() {
			Base.Route.addRoute('/', function(id) {
				$.get('/tpl/lists.html', function(data) {
					$('.g-bd').html(data);
				});
			});
			Base.Route.addRoute('/{id}', function(id) {
				require(['app/' + id + '/index'], function(Index) {
					new Index(id);
				});
			});
		}*/
	});

	Base.Hash = Hasher;
	Base.Route = Crossroads;

	return Base;
});

