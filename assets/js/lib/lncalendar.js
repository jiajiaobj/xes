(function($) {
	var CONFIG;
	var LC = {
		name: "ln.calendar",
		version: '1.0',
		type: 'month',
	};
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var weeks = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];

	$.fn.lncalendar = function(options) {
		var options = CONFIG = $.extend(LC, options);
		this.each(function(i, _element) {
			var element = $(_element);
			var date = new Date(); //默认时间为当天
			var type = options.View.type = LC.type; //设置视图类型
			options.View.renderView(date, function(html) {
				element.empty().append(html);
			});

			//前翻
			$(document).on('click', ".lc-header #prev", function() {
				if (type == 'month') {
					date = new Date(date.getFullYear(), date.getMonth() - 1, 1);
				} else if (type == 'week') {
					date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
				}
				options.View.renderView(date, function(html) {
					element.empty().append(html);
				})
			});
			//后翻
			$(document).on('click', ".lc-header #next", function() {
				if (type == 'month') {
					date = new Date(date.getFullYear(), date.getMonth() + 1, 1);
				} else if (type == 'week') {
					date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7);
				}
				options.View.renderView(date, function(html) {
					element.empty().append(html);
				})
			});
			//今天
			$(document).on('click', ".lc-header #today", function() {
				date = new Date();
				options.View.renderView(date, function(html) {
					element.empty().append(html);
				})
			});

		});
		return this;
	};

	//日程表 
	var View = LC.View = ({
		language: null,
		type: null,
		current_time: null,
		initialize: function(date) {
			this.current_time = new Date(date);
		},
		renderView: function(date, fn) {
			var __self__ = this;
			var view;
			var headerHtml = $("<div class='lc-header clearfloat animated fadeIn'></div>");
			this.initialize(date);
			if (__self__.type == 'month') {
				view = __self__.monthView;
				headerHtml.addClass('lc-wave');
			} else if (__self__.type == 'week') {
				view = __self__.weekView;
			}
			view.renderContent(this.current_time, function(html) {
				var str = $("<div class='ln-calendar animated fadeIn'></div>");
				var popover = $("<div id='lc-popover' class='animated fadeIn'></div>");
				headerHtml.append("<span id='title' class='left'>" + view.title + "</span>");
				headerHtml.append("<button id='prev' class='left lc-icon lc-icon-left' type='button'></button><button id='next' class='left lc-icon lc-icon-right' type='button'></button><button id='today' class='right' type='button'>Today</button>");

				popover.append("<div id='arrow'></div><div id='container'></div>").appendTo(html);

				str.append(headerHtml).append(html);
				if (fn) {
					fn(str);
				}
			});
		}
	});
	//月历
	var monthView = LC.View.monthView = ({
		title: null,
		start: null,
		end: null,
		len: null,
		month: null,
		year: null,

		initialize: function(current) {
			var d = new Date(current);
			var m = d.getMonth();
			var y = d.getFullYear();
			var l = [(new Date(y, m + 1, 1)).getTime() - (new Date(y, m, 1)).getTime()] / (24 * 3600 * 1000);
			this.title = months[m] + "&nbsp;" + y;
			this.month = m;
			this.year = y;
			this.start = new Date(y, m, 1);
			this.len = l;
			this.end = new Date(y, m, this.len);
		},
		renderContent: function(current, fn) {
			var __self__ = this;
			this.initialize(current);
			$.get(CONFIG.source, {
				startDate: __self__.start,
				endDate: __self__.end,
				userId:1,
				userType:'student'
			}, function(data) {
				var courseData=data.data.course;
				var table = $("<table id='lc-content'></table>");
				table.append(__self__.renderThead()).append(__self__.renderTbody(courseData));
				if (fn) {
					fn(table);
				}
			});
			//return table;
		},
		renderThead: function() {
			var thead = $("<thead></thead>");
			var th = $("<tr></tr>");
			for (var i = 0; i < weeks.length; i++) {
				th.append("<th>" + weeks[i] + "</th>");
			}
			thead.append(th);
			return thead;
		},
		renderTbody: function(data) {
			var start = this.start;
			var len = this.len;
			var sw = start.getDay();
			start = new Date(start.getTime() - sw * 24 * 3600 * 1000);
			var tbody = $("<tbody></tbody>");
			var tr = $("<tr></tr>");

			//月历主体
			var count = 0;
			var real_today = new Date();
			for (var j = 0; j < 42; j++) {
				var td = $("<td></td>");
				var day = new Date(start.getTime() + j * 24 * 3600 * 1000);
				var span = $("<span class='lc-date'></span>");
				span.html(day.getDate());
				var date_div = $("<div class='lc-date_div'></div>");

				if (day.toDateString() == real_today.toDateString()) {
					td.addClass('lc-table-highlight');
				}
				var key = day.getFullYear() + "-" + (parseInt(day.getMonth()) + 1) + "-" + day.getDate();
				td.append(date_div.append(span));
				if (j >= sw && j < (this.len + sw)) {
					td.find(".lc-date").css('color', 'black');
					if (data[key] != null) {
						td.append(this.renderEvent(data[key]));
					}
				}

				tr.append(td);
				count++;
				if (count % 7 == 0 && j < 42) {
					tbody.append(tr.clone(true));
					tr.empty();
				}
			}
			tbody.find('tr').css('height', 530 / 6);
			tbody.find('.lc-event_list').css('height', 530 / 6 - 25);

			return tbody;
		},
		renderEvent: function(data) {
			var lc_event = $("<div class='lc-event_list'></div>");
			for (var i = 0; i < data.length; i++) {
				var ev = $("<span class='lc-event'></span>");

				ev.attr('id', i);
				ev.html(data[i].startTime + "-" + data[i].endTime);
				events_detail(ev, data);
				lc_event.append(ev);
			}
			return lc_event;
		}
	});
	//周历
	var weekView = LC.View.weekView = {
		start: null,
		end: null,
		title: null,
		initialize: function(current) {
			var d = new Date(current);
			var m = d.getMonth();
			var y = d.getFullYear();
			this.title = months[m] + "&nbsp;" + y;
			this.start = new Date(y, m, (d.getDate() - d.getDay() + 1));
			this.end = new Date(y, m, (d.getDate() - d.getDay() + 7));
		},
		renderContent: function(current, fn) {
			this.initialize(current);
			var ul = $("<ul class='full_wide clearfloat' id='lc-content'></ul>");

			var d = this.start;
			var y = d.getFullYear();
			var m = d.getMonth();
			var day = d.getDate();
			var real_today = new Date();

			var __self__ = this;
			$.get(CONFIG.source, {
				startDate: new Date(y,m,day),
				endDate: new Date(y,m,day+7),
				userId:1,
				userType:'teacher'
			}, function(data) {
				var courseData=data.data.course;
				for (var i = 0; i < 7; i++) {
					var li = $("<li></li>");

					d = new Date(y, m, day + i);
					var str = "<div class='lc-date'>" + __self__.renderDate(d) + "</div>";
					if (real_today.getFullYear() == y && real_today.getMonth() == m && real_today.getDate() == (day + i)) {
						li.addClass("lc-li-highlight").append(str);
					} else {
						li.append(str);
					}
					var key = y + "-" + (m + 1) + "-" + (day + i);
					if (courseData[key] != null) {
						li.append(__self__.renderEvent(courseData[key]));
					}
					ul.append(li);
				}
				if(fn){
					fn(ul);
				}
			});
		},
		renderDate: function(d) {
			var str = "<span class='number'>" + d.getDate() + "</span>" + "<span class='week'>" + weeks[d.getDay()] + "</span>";

			return str;
		},
		renderEvent: function(data) {
			var morning = $("<div class='lc-section'></div>");
			var afternoon = $("<div class='lc-section'></div>");
			var evening = $("<div class='lc-section'></div>");
			for (var i = 0; i < data.length; i++) {
				var t = data[i];

				var ev = $("<span class='lc-event'></span>");
				ev.attr('id', i);
				ev.html(t.startTime + "-" + t.endTime);
				events_detail(ev, data);

				var s = t.startTime.split(":")[0];
				if (s < 12) {
					morning.append(ev);
				} else if (s >= 12 && s < 18) {
					afternoon.append(ev);
				} else {
					evening.append(ev);
				}
			}
			var div = $("<div class='event_div'></div>");
			div.append(morning).append(afternoon).append(evening);
			return div;
		},
	};
	//events点击事件
	function events_detail(ev, data) {
		ev.on('click', function() {
			var e_id = data[this.id];
			popover_content(e_id);
			popover_position(this);

			$(this).closest("#lc-content").find(".lc-event").removeClass("lc-event-highlight");
			$(this).addClass("lc-event-highlight");
			
			$("#lc-popover").show();

		});
		document.onclick = function(event) {
			var e = event || window.event;
			var elem = e.srcElement || e.target;

			if (elem.className.indexOf("lc-event") < 0) {
				if ($(elem).closest("#lc-popover").length == 0) {
					$("#lc-popover").hide();
					$("#lc-content").find(".lc-event").removeClass("lc-event-highlight");
				}
			}
		}
	}
	//popover内容
	function popover_content(events) {
		var img="<img src='"+events.course_img+"'class='course_img'/>";
		var title = "<h4 class='counts'>第"+events.number+"/"+events.counts+"节</h4><h4>" + events.title + "</h4><p class='section'>"+events.startDate+"-"+events.endDate+"</p>";
		var label = "<label class='clearfloat people_time'><span class='left'>" + events.person + "</span><span class='right'>" + events.startTime + "-" + events.endTime + "</span></label>";
		$("#lc-popover #container").empty().append(img).append(title).append(label);
		var button=$("<a id='enter_room' class='cloud_button a-light' href='"+events.enter_url+"'></a>");
		if(events.type=="video"){
			button.html("回看视频");
			$("#lc-popover #container").append(button);
		}else if(events.type=="room"){
			button.html("进入教室");
		}
	}
	//popover位置，尺寸
	function popover_position(obj) {
		var eve = $(obj);
		var ew = eve.width();
		var e_l = eve.offset().left;
		var e_t = eve.offset().top;

		var pop_h = $("#lc-popover").innerHeight();
		var pop_w = $("#lc-popover").innerWidth();
		var dw = $(document).width();
		var dh = $(document).height();
		var x;
		var y;
		var direction = 1;
		var arrow_y;
		var arrow_x;

		var pop_r = dw - e_l - ew;
		var pop_b = dh - e_t;


		if (pop_r > pop_w && pop_b > pop_h) {
			x = e_l + ew;
			y = e_t;
			arrow_x = 0;
			arrow_y = 0;
			direction = 1;
		} else if (pop_r <= pop_w && pop_b > pop_h) {
			x = e_l - pop_w;
			y = e_t;
			arrow_x = pop_w - 10;
			arrow_y = 0;
			direction = 0;
		} else if (pop_r > pop_w && pop_b <= pop_h) {
			x = e_l + ew;
			y = dh - pop_h;
			arrow_x = 0;
			arrow_y = e_t - y;
			direction = 1;
		}else if (pop_r <= pop_w && pop_b <= pop_h) {
			x = e_l - pop_w;
			y = dh - pop_h;
			arrow_x = pop_w - 10;
			arrow_y = e_t - y;
			direction = 0;
		}
		$("#lc-popover").css({
			"top": y - 15,
			"left": x
		});
		$("#lc-popover #arrow").css({
			'top': arrow_y + 10,
			'left': arrow_x
		});
		if (direction) {
			$("#lc-popover #arrow").removeClass("lc-arrow-right").addClass("lc-arrow-left");
		} else {
			$("#lc-popover #arrow").removeClass("lc-arrow-left").addClass("lc-arrow-right");
		}
	};
})(jQuery);

