var fe = require('file-extension');
var multer = require('multer');
var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, __dirname + '/../uploads')
	},
	filename: function(req, file, cb) {
		var ex = fe(file.originalname);
		console.log(req.files);
		cb(null, Date.now() + '.' + ex)
	}
});
var upload = multer({
	storage: storage
});
module.exports = function(app) {
	app.get('/', function(req, res) {
		res.render('index.html', {
			layout: false
		});
	});
	app.get('/constructor', function(req, res) {
		res.render('constructor.html', {
			layout: false
		})
	});
	app.get('/callback', function(req, res) {
		res.render('callback.html', {
			layout: false
		})
	});
	app.get('/mustgo', function(req, res) {
		res.render('mustgo.html', {
			layout: false
		})
	});
	app.get('/nodejs', function(req, res) {
		res.render('nodejs.html', {
			layout: false
		})
	});

	app.get('/form', function(req, res) {
		res.render('form.html', {
			layout: false
		})
	})
	//post===============
	app.post('/form', upload.any(), function(req, res) {
		var name = req.body.username;
		res.redirect('/form');
	});
}

