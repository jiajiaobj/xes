var express = require("express");
var partials = require('express-partials');
var fs = require("fs");
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(partials()); //layout
app.use(express.static(__dirname + "/assets"));
app.engine(".html", require("ejs").renderFile);
app.set("views", __dirname + "/views");
app.set("view engine", "html");

app.use(function(req, res, next){
	var json = {
		host:'',
		user:''
	}
	console.log('log:' + JSON.stringify(json));
	next();
})
//controller
fs.readdirSync(__dirname + '/controller').forEach(function(name) {
	if (/Controller\.js$/.test(name)) {
		require(__dirname + '/controller/' + name)(app);
	}
});

app.listen(80);

